package com.java.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;

public class ServiceSocket {

    public static void main(String[] args) throws IOException {
        // 监听指定窗口
        DatagramSocket ds = new DatagramSocket(6666);

        while (true) {
            System.out.println("服务已开启....");
            // 数据缓冲区
            byte[] buffer = new byte[1024];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            // 收取一个udp数据包
            ds.receive(packet);
            // 收取到的数据存在buffer中，由packet.getOffset(),packet.getLength()指定起始位置和长度
            String s = new String(packet.getData(), packet.getOffset(), packet.getLength(), StandardCharsets.UTF_8);
            System.out.println("客户端发来的内容：" + s);
            // 发送数据
            byte[] data = "ACK".getBytes(StandardCharsets.UTF_8);
            packet.setData(data);
            ds.send(packet);
        }
    }

}
